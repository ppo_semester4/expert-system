import bmi
import water
import calories
import fat
import full_diagnostic
from tkinter import *


def all_program():

    def choice_bmi():
        window.destroy()
        bmi.run_bmi_calculator()

    def choice_water():
        window.destroy()
        water.run_water_calculator()

    def choice_calories():
        window.destroy()
        calories.run_calories_calculator()

    def choice_fat():
        window.destroy()
        fat.run_fat_percentage_calculator()

    def choice_full_diagnostic():
        window.destroy()
        full_diagnostic.run_full_diagnostic()

    def exit_program():
        window.destroy()

    window = Tk()
    window.title('Экспертная система')
    window.geometry('500x500')

    img = PhotoImage(file="C:/Users/user/Desktop/PPO4semestr/sprint_4/cat.png")

    label = Label(
        window,
        image=img
    )
    label.place(x=0, y=0)

    frame = Frame(
        window,
        padx=10,
        pady=10,
        bg='#f7dff3'
    )
    frame.pack(expand=True)

    bmi_button = Button(
        frame,
        text="Показатель ИМТ",
        bg="#ada3ac",
        command=choice_bmi
    )
    bmi_button.grid(row=6, column=1, pady=5)

    water_button = Button(
        frame,
        text="Норма воды в сутки",
        bg="#ada3ac",
        command=choice_water
    )
    water_button.grid(row=12, column=1, pady=5)

    calories_button = Button(
        frame,
        text="Норма калорий в сутки",
        bg="#ada3ac",
        command=choice_calories
    )
    calories_button.grid(row=12, column=2, pady=5)

    fat_button = Button(
        frame,
        text="Процент жира в организме",
        bg="#ada3ac",
        command=choice_fat
    )
    fat_button.grid(row=6, column=2, pady=5)

    full_diagnostics_button = Button(
        frame,
        text="Полная диагностика",
        bg="#ada3ac",
        command=choice_full_diagnostic
    )
    full_diagnostics_button.grid(row=18, column=1, pady=5)

    exit_button = Button(
        frame,
        text="Выйти",
        bg="#ada3ac",
        command=exit_program
    )
    exit_button.grid(row=18, column=2, pady=5)

    window.mainloop()


if __name__ == '__main__':
    all_program()
