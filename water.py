from tkinter import *
from tkinter import messagebox
import main


def run_water_calculator():

    def calculate_water():
        if (not weight_tf.get().isdigit()) or (weight_tf.get().isdigit() and weight_tf.get()<0):
            print(weight_tf.get().isdigit())
            messagebox.showinfo('error', 'Некорректный ввод')
            return
        kg = int(weight_tf.get())
        water = kg * 0.03
        water = round(water, 1)
        messagebox.showinfo('water', f'Вам нужно выпивать {water} мл воды в сутки')

    def back_to_main_menu():
        window2.destroy()
        main.all_program()

    window2 = Tk()
    window2.title('Калькулятор нормы воды')
    window2.geometry('500x500')
    img = PhotoImage(file="C:/Users/user/Desktop/PPO4semestr/sprint_4/cat.png")
    label = Label(
        window2,
        image=img
    )
    label.place(x=0, y=0)

    frame2 = Frame(
        window2,
        padx=10,
        pady=10,
        bg='#f7dff3'
    )
    frame2.pack(expand=True)

    weight_lb = Label(
        frame2,
        text="Введите свой вес (в кг)  ",
        bg='#f7dff3'
    )
    weight_lb.grid(row=4, column=1)

    weight_tf = Entry(
        frame2,
        bg="#ada3ac"
    )
    weight_tf.grid(row=4, column=2, pady=7)

    start_water = Button(
        frame2,
        text='Рассчитать норму воды',
        command=calculate_water,
        bg="#ada3ac"
    )
    start_water.grid(row=6, column=2)

    back_button = Button(
        frame2,
        text='Назад',
        command=back_to_main_menu,
        bg="#ada3ac"
    )
    back_button.grid(row=6, column=1)

    window2.mainloop()
